<?php
/**
 * ecodocx functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ecodocx
 */

/*Editor-styles.css*/
/*function my_theme_add_editor_styles() {
	add_editor_style( 'editor-styles.css' );
}
add_action( 'current_screen', 'my_theme_add_editor_styles' );*/

		if ( ! function_exists( 'ecodocx_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ecodocx_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ecodocx, use a find and replace
		 * to change 'ecodocx' to the name of your theme in all the template files.
		 */
		//load_theme_textdomain( 'ecodocx', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails', array('post', 'page') );

		// Add featured image sizes
		add_image_size( 'small', 320, 600, true );
		add_image_size( 'small_300x300', 300, 300, true );
		add_image_size( 'medium', 360, 210, true );
		add_image_size( 'medium_large', 470, 210, true );
		add_image_size( 'large', 768, 600, true );

		// Register the three useful image sizes for use in Add Media modal
		/*add_filter( 'image_size_names_choose', 'wpshout_custom_sizes' );
		function wpshout_custom_sizes( $sizes ) {
			return array_merge( $sizes, array(
				'medium-width' => __( 'Medium Width' ),
				'medium-height' => __( 'Medium Height' ),
				'medium-something' => __( 'Medium Something' ),
			) );
		}*/

		// This theme uses wp_nav_menu() in one location.

		//require_once('class-walker-nav-menu-small.php');

		/*register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'ecodocx' ),
		) );*/

		register_nav_menus( array(
			'language-menu' => __( 'Language Menu', 'ecodocx' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		/*add_theme_support( 'custom-background', apply_filters( 'ecodocx_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );*/

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'ecodocx_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
/*function ecodocx_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ecodocx_content_width', 640 );
}
add_action( 'after_setup_theme', 'ecodocx_content_width', 0 );*/

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ecodocx_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ecodocx' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ecodocx' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
		'before_text'    => '<p class="widget-text">',
		'after_text'   => '</p>'
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar footer', 'ecodocx' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'ecodocx' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
		'before_text'    => '<p class="widget-text">',
		'after_text'   => '</p>'
	) );
}
add_action( 'widgets_init', 'ecodocx_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ecodocx_style() {
	wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Exo:400,500,700');
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');

	wp_enqueue_style( 'owl-carousel-style', get_template_directory_uri() . '/css/owl.carousel.min.css');
	wp_enqueue_style( 'owl-carousel-theme-style', get_template_directory_uri() . '/css/owl.theme.default.min.css');

	wp_enqueue_style( 'ecodocx-style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'ecodocx_style' );
function ecodocx_scripts() {
	/*wp_enqueue_script( 'ecodocx-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'ecodocx-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );*/
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr-custom.js', '' , '3.5.0');
	wp_script_add_data( 'modernizr', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'jquery-1.9.0', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.0/jquery.min.js', array(), '1.9.0', false );
	wp_script_add_data( 'jquery-1.9.0', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '2.2.1', false );

	wp_enqueue_script( 'bootstrap-colequalizer', get_template_directory_uri() . '/js/bootstrap-colequalizer.js', array('jquery'), '', false );

	wp_enqueue_script( 'owl-carousel-settings', get_template_directory_uri() . '/js/owl.carousel.settings.js', array('owl-carousel'), '1.0', false );

	wp_enqueue_script( 'menu', get_template_directory_uri() . '/js/menu.js', array('scrollreveal'), '1.0', false );

	wp_enqueue_script( 'parallax', get_template_directory_uri() . '/js/parallax.min.js', array('jquery'), '1.0', false );

	wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array('jquery'), '1.0', false );

	wp_enqueue_script( 'scrollreveal', 'https://unpkg.com/scrollreveal/dist/scrollreveal.min.js', array('jquery'), '', true );

	wp_enqueue_script( 'html5shiv', 'https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js');
	wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'respond', 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js');
	wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ecodocx_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}



/**
 * Stop WordPress converting quotes to pretty quotes
 */
remove_filter('the_content', 'wpautop');
remove_filter('comment_text', 'wpautop');
remove_filter('the_excerpt', 'wpautop');

/**
 * Define the different language shortcodes
 */
$language = array("html", "css", "javascript", "php");

/**
 * Create the different shortcodes
 */
foreach($language as $lang){
	add_shortcode( $lang, 'paulund_highlight_code' );
}

/* php in post or page WordPress: [exec]code[/exec]  */
function exec_php($matches){
	eval('ob_start();'.$matches[1].'$inline_execute_output = ob_get_contents();ob_end_clean();');
	return $inline_execute_output;
}
function inline_php($content){
	$content = preg_replace_callback('/\[php\]((.|\n)*?)\[\/php\]/', 'exec_php', $content);
	$content = preg_replace('/\[php off\]((.|\n)*?)\[\/php\]/', '$1', $content);
	return $content;
}
add_filter('the_content', 'inline_php', 0);

/*Excerpt Length Control*/
function set_excerpt_length() {
	return 20;
}

add_filter('excerpt_length', 'set_excerpt_length');

/*
 * add [...]
 */
function new_excerpt_more($more) {
	return '<a href="'. get_permalink($post->ID) . '">' . ' ...' . '</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');


/*
 * ALLOW SPAN TAG IN WORDPRESS EDITOR
  */
function override_mce_options($initArray)
{
	$opts = '*[*]';
	$initArray['valid_elements'] = $opts;
	$initArray['extended_valid_elements'] = $opts;
	return $initArray;
}
add_filter('tiny_mce_before_init', 'override_mce_options');