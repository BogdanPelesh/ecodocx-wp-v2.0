jQuery(document).ready(function()
{
    jQuery('input, textarea').each(function()
    {
        if (jQuery(this).attr('placeholder') && jQuery(this).attr('placeholder') != '')
        {
            jQuery(this).attr( 'data-placeholder', jQuery(this).attr('placeholder') );
        }
    });

    jQuery('input, textarea').focus(function()
    {
        if (jQuery(this).attr('data-placeholder') && jQuery(this).attr('data-placeholder') != '')
        {
            jQuery(this).attr('placeholder', '');
        }
    });

    jQuery('input, textarea').blur(function()
    {
        if (jQuery(this).attr('data-placeholder') && jQuery(this).attr('data-placeholder') != '')
        {
            jQuery(this).attr('placeholder', jQuery(this).attr('data-placeholder'));
        }
    });
});