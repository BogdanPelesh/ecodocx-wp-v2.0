<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ecodocx
 */

?>
<?php if ( get_edit_post_link() ) : ?>
    <div class="edit-link">
        <i class="fa fa-pencil-square-o"></i>
		<?php
		edit_post_link(
			sprintf(
				wp_kses(
					__( 'Edit Post', 'ecodocx' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			)
		);
		?>
    </div>
<?php endif; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ecodocx' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->

<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
    <div id="sidebar_footer">
		<?php dynamic_sidebar( 'sidebar-2' ); ?>
    </div>
<?php endif; ?>
