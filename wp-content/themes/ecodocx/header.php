<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <title><?php wp_title(); ?></title>
	<?php wp_head(); ?>
	<?php if ( is_front_page() ) : ?>
        <style type="text/css">
            #menu-item-227, #menu-item-2342, #menu-item-2360 {
                display: none;
            }
        </style>
	<?php endif; ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57373029-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-57373029-1');
    </script>
</head>
<body>
<header class="<?php if ( is_user_logged_in() && is_admin_bar_showing() ) { echo 'logged-in'; } ?>">
    <div class="top-nav">
        <div class="container">
            <div class="logo">
				<?php the_custom_logo(); ?>
            </div>
            <div id="burger-menu">
                <i class="fa fa-bars"></i>
            </div>
			<?php
			wp_nav_menu( array(
					'menu'              => 'ecodocx-menu-small-language',
					'theme_location'    => 'primary',
					'depth'             => 5,
					'container'         => 'div',
					'container_class'   => 'header-menu-small-language',
					'container_id'      => '',
					'menu_class'        => 'nav-menu-small-language',
					'fallback_cb'       => '',
					'walker'            => ''
				)
			); ?>
            <div class="utility-nav">
                <form role="search" method="get" id="searchform" action="https://ecodocx.com">
                    <input type="text" value="" name="s" id="s" placeholder="Search">
                    <button type="submit" id="searchsubmit" value="Search">
                        <i class="fa fa-search"></i>
                    </button>
                </form>
				<?php wp_nav_menu( array(
						'menu'              => 'ecodocx-menu-language',
						'theme_location'    => 'primary',
						'depth'             => 5,
						'container'         => 'div',
						'container_class'   => 'header-menu-language',
						'container_id'      => '',
						'menu_class'        => 'nav-menu-language',
						'fallback_cb'       => '',
						'walker'            => ''
					)
				); ?>
            </div>
        </div>
    </div>
    <nav class="page-nav">
        <div class="container">
			<?php
			wp_nav_menu( array(
					'menu'              => 'primary',
					'theme_location'    => 'primary',
					'depth'             => 5,
					'container'         => 'div',
					'container_class'   => '',
					'container_id'      => '',
					'menu_class'        => 'nav-menu',
					'fallback_cb'       => '',
					'walker'            => ''
				)
			);
			?>
        </div>
    </nav>
	<?php
	wp_nav_menu( array(
    'menu'              => 'ecodocx-menu-small',
    'theme_location'    => 'ecodocx-menu-small',
    'depth'             => 5,
    'container'         => 'div',
    'container_class'   => 'header-menu-small',
    'container_id'      => '',
    'menu_class'        => 'nav-menu-small',
    'fallback_cb'       => '',
    'walker'            => ''
    )
    );
    ?>

</header>
