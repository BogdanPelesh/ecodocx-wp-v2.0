<?php
/*
Plugin Name: Subscribe form
Plugin URI:  -
Description: Plugin Subscribe form from Ecodocx
Version:     1.0
Author:      Bogdan Pelesh
Author URI:  https://developer.wordpress.org/
License:     GPL2
License URI: https://www.gnu.or`g/licenses/gpl-2.0.html
Text Domain: wporg
Domain Path: /languages
*/

add_action('widgets_init', 'ecodocx_subscribe');

function ecodocx_subscribe() {
	register_widget('ECODOCX_Subscribe');
}

class ECODOCX_Subscribe extends  WP_Widget {
	public function __construct() {
		$args = array (
			'name' => 'Subscribe form',
			'description' => 'Widget Subscribe form from Ecodocx'
		);
		parent::__construct( 'ecodocx_subscribe', '', $args );
	}

	public function form($instance){
		$title = isset( $instance['title'] ) ? $instance['title'] : false;
		$text = isset( $instance['text'] ) ? $instance['text'] : false;
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"
			       id="<?php echo $this->get_field_id('title'); ?>" value="<?php echo $title; ?>" class=""widefat">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('text'); ?>">Text:</label>
			<input type="text" name="<?php echo $this->get_field_name('text'); ?>"
			       id="<?php echo $this->get_field_id('text'); ?>" value="<?php echo $text; ?>" class=""widefat">
		</p>
		<?php
	}

	public function widget($args, $instance) {
	    ?>
        <?php echo $args['before_widget']; ?>
		<?php
		    echo $args['before_title'];
            echo $instance[ 'title' ];
		    echo $args['after_title'];
        ?>
		<p><?php echo $instance[ 'text' ]; ?></p>
		<?php echo do_shortcode('[contact-form-7 id="1591" title="contact-form-blog-subscribe"]') ?>
        <?php echo $args['after_widget']; ?>
	<?php
	}

	/*public  function update(){

	}*/
}