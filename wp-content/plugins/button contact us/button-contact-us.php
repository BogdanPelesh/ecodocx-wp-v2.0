<?php
/*
Plugin Name: Button contact us
Plugin URI:  -
Description: Plugin button contact us from Ecodocx
Version:     1.0
Author:      Bogdan Pelesh
Author URI:  https://developer.wordpress.org/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wporg
Domain Path: /languages
*/

add_action('widgets_init', 'ecodocx_Contact');

function ecodocx_contact() {
	register_widget('ECODOCX_Contact');
}

class ECODOCX_Contact extends  WP_Widget {
	public function __construct() {
		$args = array (
			'name' => 'Button contact us',
			'description' => 'Widget button contact us from Ecodocx'
		);
		parent::__construct( 'ecodocx_contact', '', $args );
	}

	public function form($instance){
		$title = isset( $instance['title'] ) ? $instance['title'] : false;
		$link = isset( $instance['link'] ) ? $instance['link'] : false;
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"
			       id="<?php echo $this->get_field_id('title'); ?>" value="<?php echo $title; ?>" class=""widefat">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('link'); ?>">URL:</label>
			<input type="text" name="<?php echo $this->get_field_name('link'); ?>"
			       id="<?php echo $this->get_field_id('link'); ?>" value="<?php echo $link; ?>" class=""widefat">
		</p>
		<?php
	}

	public function widget($args, $instance) {
		?>
		<?php echo $args['before_widget']; ?>
		<a href="<?php echo $instance[ 'link' ]; ?>" class="btn-info btn-sidebar"> <?php echo $instance[ 'title' ]; ?> </a>
		<?php echo $args['after_widget']; ?>
		<?php
	}

	/*public  function update(){

	}*/
}