<?php
/*
Plugin Name: Brochure form
Plugin URI:  -
Description: Plugin Brochure form from Ecodocx
Version:     1.0
Author:      Bogdan Pelesh
Author URI:  https://developer.wordpress.org/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wporg
Domain Path: /languages
*/

add_action('widgets_init', 'ecodocx_brochure');

function ecodocx_brochure() {
	register_widget('ECODOCX_Brochure');
}

class ECODOCX_Brochure extends  WP_Widget {
	public function __construct() {
		$args = array (
			'name' => 'Brochure form',
			'description' => 'Widget brochure form from Ecodocx'
		);
		parent::__construct( 'ecodocx_brochure', '', $args );
	}

	public function form($instance){
		$title = isset( $instance['title'] ) ? $instance['title'] : false;
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"
			       id="<?php echo $this->get_field_id('title'); ?>" value="<?php echo $title; ?>" class=""widefat">
		</p>
		<?php
	}

	public function widget($args, $instance) {
		?>
		<?php echo $args['before_widget']; ?>
		<?php
		echo $args['before_title'];
		echo $instance[ 'title' ];
		echo $args['after_title'];
		?>
		<?php echo do_shortcode('[contact-form-7 id="1603" title="contact-form-blog-brochure"]') ?>
		<p class="title-small">Privacy</p>
		<p class="text-small">We respect your privacy, and will not share your information with any 3rd party without your permission.
			Our multi-level corporate security policies and procedures ensure prevention from loss, misuse or
			unauthorized distribution of any business-sensitive information you share with us.</p>
		<?php echo $args['after_widget']; ?>
		<?php
	}

	/*public  function update(){

	}*/
}