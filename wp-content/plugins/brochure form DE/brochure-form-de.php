<?php
/*
Plugin Name: Brochure form (DE)
Plugin URI:  -
Description: Plugin Brochure form from Ecodocx (DE)
Version:     1.0
Author:      Bogdan Pelesh
Author URI:  https://developer.wordpress.org/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wporg
Domain Path: /languages
*/

add_action('widgets_init', 'ecodocx_brochure_de');

function ecodocx_brochure_de() {
	register_widget('ECODOCX_Brochure_DE');
}

class ECODOCX_Brochure_DE extends  WP_Widget {
	public function __construct() {
		$args = array (
			'name' => 'Brochure form (DE)',
			'description' => 'Widget brochure form from Ecodocx (DE)'
		);
		parent::__construct( 'ecodocx_brochure_de', '', $args );
	}

	public function form($instance){
		$title = isset( $instance['title'] ) ? $instance['title'] : false;
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"
			       id="<?php echo $this->get_field_id('title'); ?>" value="<?php echo $title; ?>" class=""widefat">
		</p>
		<?php
	}

	public function widget($args, $instance) {
		?>
		<?php echo $args['before_widget']; ?>
		<?php
		echo $args['before_title'];
		echo $instance[ 'title' ];
		echo $args['after_title'];
		?>
		<?php echo do_shortcode('[contact-form-7 id="3339" title="contact-form-blog-brochure_de"]') ?>
		<p class="title-small">Privatsphäre</p>
		<p class="text-small">Wir respektieren Ihre Privatsphäre und werden Ihre Daten nicht ohne Ihre Erlaubnis an Dritte weitergeben.
            Unsere mehrstufigen Sicherheitsrichtlinien und -verfahren sorgen für die Vermeidung von Verlust, Missbrauch oder
            unbefugte Verteilung von unternehmenssensiblen Informationen, die Sie mit uns teilen.
        </p>
		<?php echo $args['after_widget']; ?>
		<?php
	}

	/*public  function update(){

	}*/
}